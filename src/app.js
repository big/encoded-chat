const http = require('http');
const express = require("express");
const app = require('express')();

app.listen(3000, () => {
    console.log('Server running on port 3000');
});

const bodyParser = require('body-parser');
const path = require("path");
const fs = require("fs");
app.engine('pug', require('pug').__express)

app.set('views', __dirname + '/views');
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, "/")));
app.use(bodyParser.json({limit: '10mb'}));

app.locals.basedir = __dirname;

app.get("/", function (req, res) {
    res.render("index",
        {
            metaTitle: "Chat",
            metaAuthor: "PLOTFINDER",
            metaDescription: "A simple chat application",
        }
        );
});

const websocket = require('websocket').server;
const httpServer = http.createServer();

const clients = {};

httpServer.listen(8080, () => {
    console.log('Server is listening on port 8080');
});

console.log('http://localhost:3000');

wsServer = new websocket({
    httpServer: httpServer
});

wsServer.on("request", request => {

    const connection = request.accept(null, request.origin);
    console.log('Connection requested');

    connection.on('open', () => {
        console.log('Connection opened');
    });

    connection.on('close', () => {
        console.log('Connection closed');

        let clientId = null;
        for (let client in clients) {
            if (clients[client].connection === connection) {
                clientId = client;
            }
        }

        removeClient(clientId);
    });

    connection.on('message', (message) => {
        const result = JSON.parse(message.utf8Data);

        if (result.method === "sendMessage") {
            let clientId = result.clientId;
            let payLoad;
            if (verifyString(result.message.text))
            {
                for (let client of Object.keys(clients)) {
                    if (client !== clientId) {
                        clients[client].connection.send(JSON.stringify({
                            method: "receiveMessage",
                            error: false,
                            requestMessage: "You have received a message",
                            message: result.message
                        }));
                    }
                }

                payLoad = {
                    method: "sendMessage",
                    error: false,
                    requestMessage: "Message sent",
                    message: result.message
                };
            } else
            {
                payLoad = {
                    method: "sendMessage",
                    error: true,
                    requestMessage: "Your message contains invalid characters"
                };
            }

            const con = clients[clientId].connection;
            con.send(JSON.stringify(payLoad));
        }
    });

    console.log('Connection accepted');
    const clientId = guid();
    clients[clientId] = {
        name: capitalizeFirstLetter(sample(adjectives)) + " " + sample(colors) + " " + sample(animals),
        connection: connection,
    };

    const payLoad = {
        method: 'connect',
        clientId: clientId,
        error: false,
        requestMessage: 'Connected to server',
        name: clients[clientId].name,
    };

    connection.send(JSON.stringify(payLoad));
});

const S4 = () => {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
}

const guid = () => {
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
};

let removeClient = (clientId) => {
    delete clients[clientId];
}

let verifyString = (string) => {
    // let banString = ["\n", "\t", "\r", "\v", "\f", "\b", "<", ">", "&", "\"", "=", "\\", "*", "/", "|"]
    let banString = [];
    if (string === "" || string.length > 64 || string.length < 0) {
        return false;
    }

    for(let i = 0; i < banString.length; ++i) {
        if(string.includes(banString[i])) {
            return false;
        }
    }
    return true;
}

let sample = (array) => {
    return array[Math.floor(Math.random() * array.length)];
}

let capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

let repairString = (string) => {
    return string.replace(/[\n\t\r\v\f\b<>]/g, '');
}

let repairStringArray = (array) => {
    for (let i = 0; i < array.length; i++) {
        array[i] = repairString(array[i]);
    }
    return array;
}

let animals = fs.readFileSync(__dirname + '/res/animals', 'utf8').split('\n');
animals = repairStringArray(animals);
let colors = fs.readFileSync(__dirname + '/res/colors', 'utf8').split('\n');
colors = repairStringArray(colors);
let adjectives = fs.readFileSync(__dirname + '/res/adjectives', 'utf8').split('\n');
adjectives = repairStringArray(adjectives);
