let name = "";
let clientId = null;

const MAX_MESSAGE_LENGTH = 64;
const MIN_MESSAGE_LENGTH = 1;

let ws = new WebSocket(`ws://${window.location.hostname}:8080`);

let chatElement = document.getElementById('chat');
let messagesElement = document.getElementById('messages-container');
let messageInputElement = document.getElementById('message-input');
let sendMessageElement = document.getElementById('send-message-button');

let encodeKeyElement = document.getElementById('encode-key');
let decodeKeyElement = document.getElementById('decode-key');

let charactersLeftElement = document.getElementById('characters-left');

let repositoryLinkImageElement = document.getElementById('repository-link-image');

repositoryLinkImageElement.onclick = () => {
    window.open('https://framagit.org/big/encoded-chat', '_blank');
}

messageInputElement.oninput = () => {
    charactersLeftElement.innerHTML = (MAX_MESSAGE_LENGTH - messageInputElement.value.length).toString();
}


let encodeMessage = (message, key) => {
    let encodedMessage = "";
    for (let i = 0; i < message.length; i++) {
        let char = message[i];
        let charCode = char.charCodeAt(0);
        let keyCode = Math.floor(key.charCodeAt(i % key.length));
        let newCharCode = charCode + keyCode - i + i % key.length;
        let newChar = String.fromCharCode(newCharCode);
        encodedMessage += newChar;
    }
    return encodedMessage;
}

let decodeMessage = (message, key) => {
    let decodedMessage = "";
    for (let i = 0; i < message.length; i++) {
        let char = message[i];
        let charCode = char.charCodeAt(0);
        let keyCode = Math.floor(key.charCodeAt(i % key.length));
        let newCharCode = charCode - keyCode + i - i % key.length;
        let newChar = String.fromCharCode(newCharCode);
        decodedMessage += newChar;
    }
    return decodedMessage;
}

sendMessageElement.onclick = () => {
    let message = messageInputElement.value;
    let key = encodeKeyElement.value;
    let encodedMessage = encodeMessage(message, key);
    sendMessage(encodedMessage);
}

sendMessage = (message) => {
    if (message.length < MIN_MESSAGE_LENGTH) {
        console.log("Message is too short");
        return;
    }
    if (message.length > MAX_MESSAGE_LENGTH) {
        console.log("Message is too long");
        return;
    }
    ws.send(JSON.stringify({
        method: 'sendMessage',
        clientId: clientId,
        message: {
            sender: name,
            text: message
        }
    }));

    messageInputElement.value = "";
    charactersLeftElement.innerHTML = MAX_MESSAGE_LENGTH.toString();
}

ws.onmessage = message => {
    const response = JSON.parse(message.data);
    if (response.method === "connect")
    {
        clientId = response.clientId;
        name = response.name;
        console.log(`Connected as ${name}`);
    }
    else if (response.method === 'sendMessage')
    {
        if (response.error) {
            console.log(response.requestMessage); //TODO show error message in UI instead of console log
        } else {
            let messageElement = document.createElement('div');
            messageElement.classList.add('message');

            messageElement.innerText = `${response.message.sender} (you) :  ${decodeMessage(response.message.text, decodeKeyElement.value)}`;

            messagesElement.appendChild(messageElement);
        }
    }
    else if (response.method === 'receiveMessage')
    {
        if (response.error) {
            console.log(response.requestMessage); //TODO show error message in UI instead of console log
        } else {
            let messageElement = document.createElement('div');
            messageElement.classList.add('message');

            messageElement.innerText = `${response.message.sender} : ${decodeMessage(response.message.text, decodeKeyElement.value)}`;

            messagesElement.appendChild(messageElement);
        }
    }
}

window.onkeydown = event => {
    if (event.key === 'Enter') {
        let message = messageInputElement.value;
        let key = encodeKeyElement.value;
        let encodedMessage = encodeMessage(message, key);
        sendMessage(encodedMessage);
        charactersLeftElement.innerHTML = MAX_MESSAGE_LENGTH.toString();
    }
}